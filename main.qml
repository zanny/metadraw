import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.15

ApplicationWindow {
    id: root
    visible: true

    minimumWidth: 400
    minimumHeight: 400

    menuBar: MenuBar {
        Menu {
            title: qsTr("&File")
            Action {
                text: qsTr("&New")
                icon.name: "document-new"
                shortcut: StandardKey.New
            }
            Action {
                text: qsTr("&Open")
                icon.name: "document-open"
                shortcut: StandardKey.Open
            }
            Action {
                text: qsTr("&Save")
                icon.name: "document-save"
                shortcut: StandardKey.Save
            }
            Action {
                text: qsTr("Save &As")
                icon.name: "document-save-as"
                shortcut: StandardKey.SaveAs
            }
        }
    }

    header: ToolBar {
        RowLayout {
            SpinBox {
                id: size
                from: 1
                to: 1000
                value: 10
            }

            ToolButton {
                Layout.fillHeight: true
                Layout.preferredWidth: height
                Rectangle {
                    anchors.centerIn: parent
                    color: colorDialog.color
                    height: parent.height * .7
                    width: height
                }
                onClicked: colorDialog.open()
            }

            Slider {
                id: opacity
                enabled: !pressurized.checked
                value: 1
                live: false
                stepSize: .01
                onValueChanged: canvas.setStyle()
                onEnabledChanged: canvas.setStyle()
            }
            ToolButton {
                id: pressurized
                text: qsTr("Pressure Opacity")
                checkable: true
                onClicked: print(canvas.actions.length, canvas.redoActions.length)
            }
            SpinBox {
                // TODO is there a clean way to keep this in sync with canvas.rotation?
                id: rotator
                from: 0
                to: 355
                stepSize: 5
                wrap: true
            }
            ToolButton {
                id: undoButton
                text: qsTr("Undo")
                enabled: false
                onClicked: canvas.undo()
            }
            ToolButton {
                id: redoButton
                text: qsTr("Redo")
                enabled: false
                onClicked: canvas.redo()
            }
        }
    }

    footer: ToolBar {
        Label { text: "Footer bar" }
    }

    Canvas {
        id: canvas
        width: 400
        height: 400
        renderStrategy: Canvas.Threaded
        renderTarget: Canvas.FramebufferObject
        rotation: rotator.value

        property var actions: []
        property var redoActions: []

        onAvailableChanged: {
            getContext('2d')
            wipe()

            context.lineJoin = 'round'
            context.lineCap = 'round'
        }

        HoverHandler {
            cursorShape: Qt.CrossCursor
        }
        TapHandler {
            gesturePolicy: TapHandler.WithinBounds
            onPressedChanged: {
                if(pressed) {
                    canvas.context.beginPath();
                    canvas.context.moveTo(point.position.x, point.position.y)
                    canvas.actions.push([point.position])
                    undoButton.enabled = true
                }
            }
            onPointChanged: {
                if(point.position.x != 0 && point.position.y != 0) {
                    if(pressurized.checked) {
                        canvas.setStyle(point.pressure)
                    }
                    canvas.segment(point.position, 
                                   size.value * point.pressure, 
                                   canvas.context.strokeStyle)

                    canvas.actions[canvas.actions.length - 1].push({ 
                        pos:  point.position, 
                        width: canvas.context.lineWidth, 
                        style: canvas.context.strokeStyle 
                    })
                }
            }
        }
        DragHandler {
            acceptedButtons: Qt.MiddleButton
            acceptedModifiers: Qt.NoModifier
            cursorShape: Qt.ClosedHandCursor
            xAxis.minimum: -canvas.width / 2
            xAxis.maximum: root.width + xAxis.minimum
            yAxis.minimum: -canvas.height / 2
            yAxis.maximum: root.height - yAxis.minimum
        }
        DragHandler {
            acceptedButtons: Qt.MiddleButton
            acceptedModifiers: Qt.ShiftModifier
            cursorShape: Qt.ClosedHandCursor
            target: null
        }
        WheelHandler {
            acceptedModifiers: Qt.ShiftModifier
            property: "rotation"
        }
        DragHandler {
            acceptedButtons: Qt.MiddleButton
            acceptedModifiers: Qt.ControlModifier
            cursorShape: Qt.ClosedHandCursor
            target: null
        }
        WheelHandler {
            acceptedModifiers: Qt.NoModifier
            property: "scale"
        }

        function wipe() {
            context.beginPath()
            context.rect(0, 0, width, height)
            context.fillStyle = 'white'
            context.fill();
        }

        function reconstitute(line) {
            let start = line.shift()
            canvas.context.beginPath();
            canvas.context.moveTo(start.x, start.y)
            for(const seg of line) {
                segment(seg.pos, seg.width, seg.style)
            }
            line.unshift(start)
        }

        function segment(point, width, style) {
            context.lineWidth = width
            context.strokeStyle = style
            context.lineTo(point.x, point.y)
            context.stroke()
            context.beginPath()
            context.moveTo(point.x, point.y)
            requestPaint()
        }

        function undo() {
            let tx = canvas.actions.pop()

            wipe()
            for(const line of canvas.actions) {
                canvas.reconstitute(line)
            }

            if(!canvas.actions.length) {
                undoButton.enabled = false
            }
            canvas.redoActions.push(tx)
            redoButton.enabled = true
        }

        function redo() {
            let tx = canvas.redoActions.pop()
            if(!canvas.redoActions.length) {
                redoButton.enabled = false
            }
            canvas.reconstitute(tx)
            canvas.actions.push(tx)
            undoButton.enabled = true
        }

        function setStyle(alpha) {
            if(arguments.length == 0) {
                var alpha = opacity.value
            }
            context.strokeStyle = Qt.rgba(colorDialog.color.r, colorDialog.color.g, colorDialog.color.b, alpha)
        }
    }

    ColorDialog {
        id: colorDialog
        onAccepted: canvas.setStyle()
    }
}
