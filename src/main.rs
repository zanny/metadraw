use cstr::cstr;
use qmetaobject::*;

qrc!(rc, "/" { "main.qml" });

fn main() {
    rc();
    let mut engine = QmlEngine::new();
    engine.load_file(":/main.qml".into());
    engine.exec();
}
